
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>

using namespace std;

//find a specific word in the given file

string procs(string dict,string faddr){
    string res=dict;
    ifstream files (faddr);
    string line;
    int sum=0;
    while (getline (files,line)){
        int found = line.rfind(dict);
        while (found!=string::npos){
        	sum++;
        	line.replace (found,dict.length(),"");
        	found = line.rfind(dict);
        }
    }
    files.close();
    string str = std::to_string(sum);
    res.append(",");
    res.append(str);
    return res;
}

int main(int argc, char* argv[])
{
    string dict;
    string dictaddr;
    string faddr;
    string optaddr="";

    //if only 2 args
    if (argc==3) {
    	string temp (argv[1]);
        dict =temp;
        string temp1 (argv[2]);
        faddr=temp1;
        cout<<procs(dict,faddr)<<endl;

    //if are more args
    }else if(argc>3){
    	ofstream out;
        int i=1;
        while (i<argc) {
        	string temp (argv[i]);

            if (temp=="-f"){ 			//open dict file
                dictaddr=argv[i+1];
                i=i+2;
            }else if (temp=="-w"){		//open output file
                optaddr=argv[i+1];
                cout<<optaddr;
                i+=2;
                out.open(optaddr);
            }else{						//open the file need to search
                faddr=argv[i];
                i++;
            }
        }
        string dictt;
        ifstream dictf(dictaddr);
        while ((getline(dictf,dictt))&&(dictt!="")){
        	if (optaddr!=""){
        		out<<procs(dictt,faddr)<<endl;			//search the file for every word in dict list
        	}else{
            cout<<procs(dictt,faddr)<<endl;}
        }
        dictf.close();
        out.close();
    }else{
    	cout<<"incorrect arguments"<<endl;				//throw out the error

    }
    return 0;
}
